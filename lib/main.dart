import 'package:flutter/material.dart';
import 'package:metacognition_question/view/login_splash.dart';

void main() {
  return runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    theme: ThemeData(
      primaryColor: Colors.green[300],
      primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
      primaryIconTheme: IconThemeData(color: Colors.white),
      accentColor: Colors.white24,
      iconTheme: IconThemeData(color: Colors.white),
    ),
  ));
}
