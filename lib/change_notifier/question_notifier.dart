import 'package:flutter/cupertino.dart';
import 'package:metacognition_question/data/meta_question_strings.dart';
import 'package:metacognition_question/dto/question_dto.dart';
import 'package:metacognition_question/provider/sql_provider.dart';
import 'package:metacognition_question/service/list_shuffle.dart';

class QuestionNotifier with ChangeNotifier {
  QuestionNotifier() {
    getQuestion();
  }
  QuestionDto questionDto;

  final QuestionDatabaseProvider _questionDatabaseProvider =
      QuestionDatabaseProvider.databaseProvider;

  getQuestion() async {
    var list = shuffle(MetaQuestionStrings.list);
    questionDto =
        QuestionDto(await _questionDatabaseProvider.getOneQuestion(list.first));
    notifyListeners();
  }
}
