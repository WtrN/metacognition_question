import 'package:flutter/material.dart';
import 'package:metacognition_question/dto/question_dto.dart';
import 'package:metacognition_question/provider/sql_provider.dart';

class AllQuestionNotifier extends ChangeNotifier {
  AllQuestionNotifier() {
    getAllQuestion();
  }

  List<QuestionDto> allQuestions = [];

  final _questionDatabaseProvider = QuestionDatabaseProvider.databaseProvider;

  getAllQuestion() async {
    final list = await _questionDatabaseProvider.getAllQuestions();
    allQuestions =
        List.generate(list.length, (index) => QuestionDto(list[index]));
    notifyListeners();
  }
}
