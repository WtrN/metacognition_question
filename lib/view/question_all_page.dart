import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metacognition_question/change_notifier/all_question_notifier.dart';
import 'package:provider/provider.dart';

class AllQuestionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AllQuestionNotifier>(
      create: (_) => AllQuestionNotifier(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'メタ認知質問一覧',
            style: Theme.of(context).primaryTextTheme.headline6,
          ),
        ),
        body: Consumer<AllQuestionNotifier>(
          builder: (BuildContext context,
                  AllQuestionNotifier allQuestionNotifier, _) =>
              SafeArea(
            child: allQuestionNotifier.allQuestions.isNotEmpty
                ? ListView.builder(
                    itemCount: allQuestionNotifier.allQuestions.length,
                    itemBuilder: (BuildContext context, int index) =>
                        _QuestionComponent(
                      questions:
                          allQuestionNotifier.allQuestions[index].questions,
                    ),
                  )
                : Container(),
          ),
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width,
        ),
      ),
    );
  }
}

class _QuestionComponent extends StatelessWidget {
  const _QuestionComponent({
    Key key,
    this.questions,
  }) : super(key: key);

  final List<String> questions;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
      padding: EdgeInsets.all(10),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(
              questions.length,
              (number) => Text(
                    questions[number],
                  ))),
    ));
  }
}
