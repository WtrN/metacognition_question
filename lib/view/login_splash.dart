import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metacognition_question/config/ad_mob_config.dart';
import 'package:metacognition_question/provider/sql_provider.dart';
import 'package:metacognition_question/view/metacognition_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreen();
}

class _SplashScreen extends State<SplashScreen> {
  final QuestionDatabaseProvider questionDatabaseProvider =
      QuestionDatabaseProvider.databaseProvider;
  bool _initialized = false;
  bool _expired = false;

  @override
  void initState() {
    super.initState();
    // admob インスタンス初期化
    FirebaseAdMob.instance.initialize(appId: getAdMobAppId());
    _startTimer();
    _initialize();
  }

  @override
  Widget build(BuildContext context) {
    final double _imageSize = MediaQuery.of(context).size.width * 0.5;

    return Scaffold(
      body: Center(
        child: Image.asset(
          'lib/assets/icon2.png',
          height: _imageSize,
          width: _imageSize,
        ),
      ),
    );
  }

  // 初期化処理
  _initialize() async {
    await questionDatabaseProvider.getDatabaseInstance();
    await questionDatabaseProvider.createQuestionsDate();
    _initialized = true;
    await _moveToMainScreen();
  }

  // スプラッシュ画面を終了させるタイマーを開始する
  _startTimer() async {
    Timer(Duration(seconds: 2), () async {
      _expired = true;
      await _moveToMainScreen();
    });
  }

  // スプラッシュ画面をメイン画面に置き換えて遷移する
  _moveToMainScreen() async {
    if (_initialized && _expired) {
      await Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => MetaCognitionPage()));
    }
  }
}
