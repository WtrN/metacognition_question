import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metacognition_question/view/question_all_page.dart';

class DrawerMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: Container(),
          ),
          _MenuItem(
            menuIcon: Icon(Icons.format_list_numbered),
            menuContent: Text('質問一覧'),
            tapAction: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => AllQuestionPage())),
          ),
          _MenuItem(
            menuIcon: Icon(Icons.format_quote),
            menuContent: Text('ライセンス'),
            tapAction: () => showLicensePage(context: context),
          )
        ],
      ),
    );
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem({
    Key key,
    this.tapAction,
    this.menuIcon,
    this.menuContent,
  }) : super(key: key);

  final VoidCallback tapAction;

  final Icon menuIcon;

  final Widget menuContent;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: menuIcon,
      title: menuContent,
      onTap: tapAction,
    );
  }
}
