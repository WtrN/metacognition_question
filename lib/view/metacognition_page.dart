import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metacognition_question/change_notifier/question_notifier.dart';
import 'package:metacognition_question/config/ad_mob_config.dart';
import 'package:metacognition_question/dto/question_dto.dart';
import 'package:metacognition_question/view/drawer_menu.dart';
import 'package:provider/provider.dart';

@immutable
class MetaCognitionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //admobのバナー広告表示
    myBanner
      ..load()
      ..show(
        anchorOffset: 0.0,
        anchorType: AnchorType.bottom,
      );

    return ChangeNotifierProvider<QuestionNotifier>(
      create: (_) => QuestionNotifier(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'メタ認知質問集',
            style: Theme.of(context).primaryTextTheme.headline6,
          ),
        ),
        drawer: DrawerMenu(),
        body: Consumer<QuestionNotifier>(
            builder: (BuildContext context, QuestionNotifier question, _) {
          return SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _questionTiles(context, question),
                _bottomButton(context, question)
              ],
            ),
          );
        }),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width,
        ),
      ),
    );
  }

  Expanded _questionTiles(BuildContext context, QuestionNotifier question) {
    return Expanded(
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 8.0, right: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: _generateListTiles(context, question.questionDto),
            ),
          ),
        ),
      ),
    );
  }

  Padding _bottomButton(BuildContext context, QuestionNotifier question) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle, color: Theme.of(context).primaryColor),
        child: IconButton(
          iconSize: 40,
          onPressed: () async {
            await question.getQuestion();
          },
          icon: Icon(
            Icons.cached,
          ),
        ),
      ),
    );
  }

  List<Widget> _generateListTiles(
      BuildContext context, QuestionDto questionDto) {
    return questionDto != null
        ? List.generate(
            questionDto.questions.length,
            (index) => Column(
              children: <Widget>[
                Container(
                  height: 120.0,
                  child: ListTile(
                    leading: Text(
                      'Q.${index + 1}',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.0),
                    ),
                    title: Center(
                      child: Text(
                        questionDto.questions[index],
                        style: TextStyle(fontSize: 20.0),
                      ),
                    ),
                  ),
                ),
                Divider(
                  indent: MediaQuery.of(context).size.width * 0.2,
                ),
              ],
            ),
          )
        : [Container()];
  }
}
