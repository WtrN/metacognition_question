import 'package:json_annotation/json_annotation.dart';
import 'package:metacognition_question/Service/question_data_sql.dart';
import 'package:sqflite/sqflite.dart';

part 'meta_cognition_question_data.g.dart';

///
/// メタ認知質問を格納するクラス
///

@JsonSerializable()
class MetaCognitionQuestion {
  const MetaCognitionQuestion({
    this.id,
    this.question,
  });

  final int id;

  final String question;

  factory MetaCognitionQuestion.fromMap(Map<String, dynamic> json) =>
      _$MetaCognitionQuestionFromJson(json);
  Map<String, dynamic> toMap() => _$MetaCognitionQuestionToJson(this);

  Future<int> addHistory(MetaCognitionQuestion meta) async {
    final QuestionDBProvider provider = QuestionDBProvider();
    final Database database = await provider.database;

    return await database.insert(provider.tableName, meta.toMap());
  }

  Future<List<Map<String, dynamic>>> checkDatabase() async {
    final QuestionDBProvider provider = QuestionDBProvider();
    final Database database = await provider.database;
    List<Map<String, dynamic>> map = await database.query(provider.tableName);

    return map;
  }
}
