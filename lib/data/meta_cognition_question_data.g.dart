// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meta_cognition_question_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MetaCognitionQuestion _$MetaCognitionQuestionFromJson(
    Map<String, dynamic> json) {
  return MetaCognitionQuestion(
    id: json['id'] as int,
    question: json['question'] as String,
  );
}

Map<String, dynamic> _$MetaCognitionQuestionToJson(
        MetaCognitionQuestion instance) =>
    <String, dynamic>{
      'id': instance.id,
      'question': instance.question,
    };
