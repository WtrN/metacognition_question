import 'package:metacognition_question/data/meta_cognition_question_data.dart';

class QuestionDto {
  List<String> questions;

  QuestionDto(MetaCognitionQuestion metaCognitionQuestion)
      : questions = metaCognitionQuestion.question.split('\n');
}
