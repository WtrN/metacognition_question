import 'dart:io';
import 'package:metacognition_question/data/meta_cognition_question_data.dart';
import 'package:metacognition_question/data/meta_question_strings.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class QuestionDatabaseProvider {
  QuestionDatabaseProvider._();

  static final QuestionDatabaseProvider databaseProvider =
      QuestionDatabaseProvider._();

  Database _database;

  String dataBasePath = 'MetaCogQuestion';
  String tableName = 'MetaCogQuestion';

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstance();
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = join(documentsDirectory.path, dataBasePath);

    return await openDatabase(path, version: 1, onCreate: _createTable);
  }

  /// table 作成
  Future<void> _createTable(Database db, int version) async {
    return await db.execute(
      '''
    CREATE TABLE $tableName(
        id INTEGER PRIMARY KEY,
        question TEXT
        )
    ''',
    );
  }

  /// メタ認知質問データベース内容作成
  createQuestionsDate() async {
    final Database db = await database;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool existDBFlag = prefs.getBool('existDBFlag') ?? false;

    if (!existDBFlag) {
      await db.transaction((transaction) async {
        Batch batch = transaction.batch();
        int id = 1;
        for (String question in MetaQuestionStrings.questions) {
          batch.insert(tableName,
              MetaCognitionQuestion(id: id, question: question).toMap());

          id += 1;
        }
        await batch.commit();
      });
      await prefs.setBool('existDBFlag', true);
    }
  }

  /// 全ての質問を取得
  Future<List<MetaCognitionQuestion>> getAllQuestions() async {
    final Database db = await database;
    List<Map<String, dynamic>> response = await db.query(tableName);
    List<MetaCognitionQuestion> result = response.isNotEmpty
        ? response.map((value) => MetaCognitionQuestion.fromMap(value)).toList()
        : [];
    return result;
  }

  /// 指定された質問IDのデータを取得
  Future<MetaCognitionQuestion> getOneQuestion(int id) async {
    final Database db = await database;
    List<Map<String, dynamic>> response = await db.query(tableName,
        columns: ['id', 'question'], where: 'id = ?', whereArgs: [id]);
    List<MetaCognitionQuestion> result = response.isNotEmpty
        ? response.map((value) => MetaCognitionQuestion.fromMap(value)).toList()
        : [];
    return result.first;
  }

  /// データベースを削除
  deleteAllDB() async {
    final db = await database;
    await db.delete(tableName);
  }
}
