import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';

String admob01 = BannerAd.testAdUnitId; //テスト用。本当はここに広告ユニットIDを設定

//admob広告表示のための初期設定
MobileAdTargetingInfo _targetingInfo = MobileAdTargetingInfo(
  keywords: <String>['DaiGo', 'パレオな男', 'メタ認知'],
  childDirected: false,
  testDevices: <String>[],
);

BannerAd myBanner = BannerAd(
  adUnitId: getAdMobAdvertiseId(), //バナー広告のユニットID
  size: AdSize.banner,
  targetingInfo: _targetingInfo,
  listener: (MobileAdEvent event) {
    print("BannerAd event is $event");
  },
);

String getAdMobAppId() {
  if (Platform.isIOS) {
    return 'ca-app-pub-9833210859141715~9871133623';
  } else if (Platform.isAndroid) {
    return 'ca-app-pub-9833210859141715~8415222354';
  }
  return '';
}

String getAdMobAdvertiseId() {
  if (Platform.isIOS) {
    return 'ca-app-pub-9833210859141715/9679561934';
  } else if (Platform.isAndroid) {
    return 'ca-app-pub-9833210859141715/4284405659';
  }

  return '';
}
