import 'dart:io';

import 'package:metacognition_question/data/meta_cognition_question_data.dart';
import 'package:metacognition_question/data/meta_question_strings.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class QuestionDBProvider {
  Database _database;
  String dataBasePath = 'MetaCogQuestion';
  String tableName = 'MetaCogQuestion';
  MetaCognitionQuestion _metaCognitionQuestion = MetaCognitionQuestion();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDB();

    /// DBにデータが存在しているか確認
    List<Map<String, dynamic>> map =
        await _metaCognitionQuestion.checkDatabase();
    await _batchInsertEvent(map);

    return _database;
  }

  _batchInsertEvent(List<Map<String, dynamic>> map) {
    if (map.isEmpty) {
      int id = 1;
      _database.transaction((value) async {
        Batch batch = value.batch();
        batch.insert(
            tableName,
            MetaCognitionQuestion(
                    id: id, question: MetaQuestionStrings.questions[0])
                .toMap());
//        for (String question in MetaQuestionStrings.questions) {
//          batch.insert(tableName,
//              MetaCognitionQuestion(id: id, question: question).toMap());
//          print(question);
//          id += 1;
//        }
        await batch.commit();
      });
    }
  }

  /// DBを初期化
  Future<Database> _initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = join(documentsDirectory.path, dataBasePath);

    return await openDatabase(path, version: 1, onCreate: _createTable);
  }

  /// table 作成
  Future<void> _createTable(Database db, int version) async {
    return await db.execute(
      '''
    CREATE TABLE $tableName(
        id INTEGER PRIMARY KEY,
        question TEXT
        )
    ''',
    );
  }
}
