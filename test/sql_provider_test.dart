import 'package:flutter_test/flutter_test.dart';
import 'package:metacognition_question/data/meta_question_strings.dart';
import 'package:metacognition_question/provider/sql_provider.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

void main() async {
  sqfliteFfiInit();
  TestWidgetsFlutterBinding.ensureInitialized();
  group('DataBaseのテスト', () {
    final QuestionDatabaseProvider questionDatabaseProvider =
        QuestionDatabaseProvider.databaseProvider;
    setUp(() async {
      await questionDatabaseProvider.getDatabaseInstance();
      await questionDatabaseProvider.createQuestionsDate();
    });
    tearDown(() async {
      await questionDatabaseProvider.deleteAllDB();
    });

    test('正しくデータを取得できること', () async {
      final target = await questionDatabaseProvider.getAllQuestions();
      expect(target[0], equals(MetaQuestionStrings.questions[0]));
    });
  });
}
